# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration

#Run2 collections
TauTriggerContentRun2 = [
"HLT_xAOD__TauJetContainer_TrigTauRecMerged",
"HLT_xAOD__TauJetContainer_TrigTauRecMergedAux.",
"LVL1EmTauRoIs",
"LVL1EmTauRoIsAux."
]

#Run3 collections
TauTriggerContentRun3 = [
"HLT_TrigTauRecMerged_MVA",
"HLT_TrigTauRecMerged_MVAAux.",
"LVL1EmTauRoIs",
"LVL1EmTauRoIsAux.",
"L1_eTauRoI",
"L1_eTauRoIAux.",
"L1_cTauRoI",
"L1_cTauRoIAux."
]


