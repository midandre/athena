#include "../TruthParticleBuilder.h"
#include "../McAodFilter.h"
#include "../McAodValidationAlg.h"
#include "../McAodTupleWriter.h"

DECLARE_COMPONENT( TruthParticleBuilder )
DECLARE_COMPONENT( McAodFilter )
DECLARE_COMPONENT( McAodValidationAlg )
DECLARE_COMPONENT( McAodTupleWriter )

