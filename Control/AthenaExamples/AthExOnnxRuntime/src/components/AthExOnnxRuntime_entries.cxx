// Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration

// Local include(s).
#include "../EvaluateModel.h"
#include "../EvaluateModelWithAthInfer.h"

// Declare the package's components.
DECLARE_COMPONENT( AthOnnx::EvaluateModel )
DECLARE_COMPONENT( AthOnnx::EvaluateModelWithAthInfer )
