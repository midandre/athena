/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

#ifndef GLOBALSIM_GENERICTOBARRAYVECTOR_H
#define GLOBALSIM_GENERICTOBARRAYVECTOR_H
CLASS_DEF( std::vector<GlobalSim::GenericTOBArray> , 23737422 , 1 )
#endif
